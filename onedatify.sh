#!/usr/bin/env sh
#
# This script is meant for quick & easy install via:
#   'curl -sSL http://packages.onedata.org/onedatify.sh | sh -s <zone_name> <space_support_token> <import>'
# or:
#   'wget -qO- http://packages.onedata.org/onedatify.sh | sh -s <zone_name> <space_support_token> <import>'
#

# 
# parse command line arguments
# function arguments:
# 
detect_curl_pipe() {
    script_name="$0"
    if [ "$script_name" = "sh" ] || [ "$script_name" = "bash" ] || [ "$script_name" = "zsh" ] \
    || [ "$script_name" = "dash" ] || [ "$script_name" = "fish" ] || [ "$script_name" = "ksh" ] \
    || [ "$script_name" = "tcsh" ] || [ "$script_name" = "csh" ]; then
        # detected invocation with curl and pipe
        return 0
    else
        # standard ./command.sh invocation
        return 1
    fi
}

pipe_usage() {
cat <<EOF
This script requirers two arguments:

${0##*/} <onezone_url> <space_support_token>

Example:
${0##*/} 'https://onedata.org' 'MDAxNWxvY2F00aW9uIG9x...'

EOF
}

parse_from_pipe() {
    if [ "$1" = "onedatify" ] ; then
        shift
        parse_new_pipe "$@"
    else 
        parse_old_pipe "$@"
    fi

}

parse_new_pipe() {
    parsed_parameters=$*
    onezone_url="$2"
    parsed_parameters="${parsed_parameters} --interactive --install "
    parsed_parameters="${parsed_parameters%% }"
    echo "$parsed_parameters"
}

parse_old_pipe() {
    onezone_url="$1"
    space_support_token="$2"
    force_import="$3"

    parsed_parameters=""

    if [ "$onezone_url" = "" ] ; then
        pipe_usage
        exit 1
    fi
    parsed_parameters="${parsed_parameters}--onezone-url $onezone_url "
    if [ "$space_support_token" = "" ] ; then
        pipe_usage
        exit 1
    fi
    parsed_parameters="${parsed_parameters}--token $space_support_token "

    if [ "$force_import" = "" ]; then
        parsed_parameters="${parsed_parameters}--import "
    fi
    parsed_parameters="${parsed_parameters}--interactive --install "
    parsed_parameters="${parsed_parameters%% }"
    echo "$parsed_parameters"
}


main() {
    if ! parsed_parameters=$(parse_from_pipe "$@"); then
        echo "Error while parsing arguments:"
        echo "$@"
        exit 1
    fi

    onedatify_onezone_url=$(echo "$parsed_parameters" | cut -f 2 -d ' ')

    # Check to see which repo they are trying to install from
    # shellcheck disable=SC2154
    if [ -z "$repo" ]; then
        repo_url="https://packages.onedata.org"
    elif [ "$repo" = "dev" ]; then
        repo_url="http://onedata-dev-packages.cloud.plgrid.pl"
        echo "Using $repo_url"
    else
        echo "Repo \"$repo\" does not exist. Please use main or dev."
        exit 1
    fi

    # Get onezone version
    if ! onezone_version=$(curl --fail --silent -k "$onedatify_onezone_url/configuration" | tr ',' '\n' | grep version | tr -d '"' | cut -d ':' -f 2); then
        echo "The command:"
        echo "curl $onedatify_onezone_url/configuration"
        echo "failed. Cannot continue, unable to acquire onezone version."
        exit 1
    fi

    # Get script to install oneprovider version for that particualr onezone or overide that version
    if [ ! -z "${ONEDATIFY_INSTALL_SCRIPT_VERSION+x}" ]; then
        onezone_version_orig="$onezone_version" ;
        onezone_version="$ONEDATIFY_INSTALL_SCRIPT_VERSION"
        echo "Default Onezone version ($onezone_version_orig) overridden with $onezone_version. Overriding Onedatify version with $ONEDATIFY_INSTALL_SCRIPT_VERSION."
    fi
    onedatify_install_script=$(echo "$onezone_version" | tr - .)
        
    if [ ! -z "${ONEDATIFY_ONEPROVIDER_VERSION+x}" ]; then
        parsed_parameters="${parsed_parameters} --oneprovider-version $ONEDATIFY_ONEPROVIDER_VERSION"
        parsed_parameters="${parsed_parameters%% }"
        echo "Overriding Onedatify's Oneprovider version with: $ONEDATIFY_ONEPROVIDER_VERSION."
    fi

    if [ ! -z "${ONEDATIFY_PACKAGE_VERSION+x}" ]; then
        parsed_parameters="${parsed_parameters} --onedatify-version $ONEDATIFY_PACKAGE_VERSION"
        parsed_parameters="${parsed_parameters%% }"
        echo "Overriding Onedatify's package version with: $ONEDATIFY_PACKAGE_VERSION."
    fi

    onedatify_install_script="onedatify_${onedatify_install_script}.sh"
    #onedatify_install_script_url="$repo_url/onedatify/$onedatify_install_script"
    onedatify_install_script_url="https://gitlab.ebi.ac.uk/soumyadip/onedata-onedatify-scripts/raw/master/onedatify_19.02.1.sh"
    curl --fail --silent -k -o "/tmp/$onedatify_install_script" "$onedatify_install_script_url"
    
    # Run the script
    echo "Downloaded Onedatify installation script /tmp/$onedatify_install_script"
    chmod +x "/tmp/$onedatify_install_script"
    # shellcheck disable=SC2086
    "/tmp/$onedatify_install_script" $parsed_parameters
}

# parse_command_line_options
# wrapped up in a function so that we have some protection against only getting
# half the file during "curl | sh"
main "$@"